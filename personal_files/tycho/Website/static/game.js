var socket = io();


//Add CSS to index.html
var link_to_style = document.createElement('link');
link_to_style.rel = 'stylesheet';
link_to_style.href = '/static/styles.css';
document.head.appendChild(link_to_style);

var link_to_style = document.createElement('link');
link_to_style.rel = 'stylesheet';
link_to_style.href = '/static/seats.css';
document.head.appendChild(link_to_style);

var current_card_section = document.getElementById("current_card_front");
var current_card_image = document.createElement("img");
current_card_image.src = 'static/Pictures/Backside.png';
current_card_image.style.width = "100%";
current_card_image.style.height = "100%";
current_card_image.id = "current_card_image";
current_card_section.appendChild(current_card_image);

var enable_movement = 0;

var enable_communication = false;

//Let the server know you are there
socket.emit('new player');

socket.on('turn over next card', function(path) {
	
	var current_card_section = document.getElementById("current_card_back");
	current_card_section.removeChild(current_card_section.firstChild);
	
	var current_card_image = document.createElement("img");
	current_card_image.src = path;
	current_card_image.style.width = "100%";
	current_card_image.style.height = "100%";
	current_card_image.id = "current_card_image";
	current_card_section.appendChild(current_card_image);

	document.getElementById('current_card').style.transform = "rotateY(180deg)";
});

socket.on('choice to client', function(player_name, choice) {
	document.getElementById('players_choice').innerHTML = player_name+": "+choice;
	document.getElementById('current_card').style.transform = "rotateY(0deg)";
	
});


socket.on('update cards on screen', function(players, id) {
	
		
		while (document.getElementById('players_cards').hasChildNodes()) {  
			document.getElementById('players_cards').removeChild(document.getElementById('players_cards').firstChild);
		}
		
		
		var percentage_for_left = 0;
		for(var j = 1; j <= players[id].cards.length-1; j++){
			var card = document.createElement("img");
			card.id = "hand_card"+j;
			card.className = "card_in_hand";
			
			card.style.left = percentage_for_left+"%";
			percentage_for_left = percentage_for_left + (80 / (players[id].cards.length-2));
			
			//add images to cards
			card.src= players[id].cards[j].path;
			card.onmouseover = function(){this.className = "card_in_hand_hover"};
			card.onmouseout = function(){this.className = "card_in_hand"};
			card.onclick = function(){socket.emit("Card to pyramide", socket.id, this.id)};
			document.getElementById('players_cards').appendChild(card);
		}; 
});



//Server does not craash uppon restart
var enable_communication_boolean = false;
var domme_counter = 0;
var connection_loss_counter = 0;
setInterval(
	function() {
		if(enable_communication_boolean){
			connection_loss_counter = 0;
		}else{
			connection_loss_counter++;
		}
		
		enable_communication_boolean = false;
		
		if(connection_loss_counter < 20){
			enable_communication = true;
		}else{
			enable_communication = false;
			document.title = "Pls refresh the page";
		}
	}

,100);

socket.on('You are allowed to communicate with the server', function(){
	enable_communication_boolean = true;
	//document.title = "Communication"+ domme_counter++;
});


// Button menu



//Start Button
var Start_Button = document.getElementById('Start');
Start_Button.innerText = "Start";
Start_Button.style.width = 50;
Start_Button.style.height = 50;

function startGame() {
	if(enable_movement){
		enable_movement = 0;
		Start_Button.innerText = "Start";
	}else{
		enable_movement = 1;
		Start_Button.innerText = "Stop";
	}
	
	if(enable_communication){
		socket.emit('start');
	};
};

Start_Button.addEventListener("click", function(){startGame();});



document.getElementById('current_card').addEventListener("click", 
	function(){
		if(enable_communication){
			socket.emit('next card', socket.id);
		};
	});
	
	
