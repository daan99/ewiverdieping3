var Domme_counter2 = 0;

//player sends all the players with their data
//amount_of_players sends 
//state tells in what state of the game we are
//id gives the id of the player whose card has to be added to the table
//pyramide_round
//pyramide_path

socket.on('table', function(players, amount_of_players, state, id, pyramide_round, pyramide_path){
	
	// Only 1 player, gives white background
	if(amount_of_players < 2){
		document.body.style.backgroundColor = "white";
	}else{
		document.body.style.backgroundColor = "black";
	};
	
	
	//Get reference to div
	var play_table = document.getElementById('table');
	var path_for_backside = "/static/Pictures/Backside.png";

	
	switch (state){
		case "Kaarten":
			//Declare variable
			
			
			var seat_number;
		
			//Calculate Angle between players and 
			// setting the starting value for drawing the players at different angles
			var Angle_between_players = 360/amount_of_players;
			var angle = (amount_of_players - players[socket.id].seat)*Angle_between_players;
			
			//Redraw all cards on the table
			if(id == "Reset"){
				
				while (play_table.hasChildNodes()) {  
					play_table.removeChild(play_table.firstChild);
				}
				
				
				// add all the seats
				for (var id in players){
					//Create seat with according seat number
					seat_number = players[id].seat;
					var Seat = document.createElement("div");
					Seat.id = "seat"+seat_number;
					Seat.className = "seat";
					
					//Create nametag to show name of the player above the seat 
					var name_tag = document.createElement("p");
					name_tag.style.position = "absolute";
					name_tag.style.top = "-35%";
					name_tag.innerHTML = players[id].name;
					
					//Add nametag to the seat
					Seat.appendChild(name_tag);
					
					
					//Starting percentage
					var percentage_for_left = 0;
					
					//Create all the images in the hand and add them to the seat
					var amount_of_cards = players[id].cards.length-1
					for(var j = 1; j <= amount_of_cards; j++){
						//create card with proper id and styling
						var card = document.createElement("img");
						card.id = "card"+j;
						card.style.left = percentage_for_left+"%"
						percentage_for_left = percentage_for_left + 20;
						card.className = "card";
						
						//add images to cards
						card.src= players[id].cards[j].path;
						
						//card gets bigger when you hover over it
						card.onmouseover = function(){this.className = "card_hover";};
						card.onmouseout = function(){this.className = "card";};
						
						//Add card to seat
						Seat.appendChild(card);
					} 
				
				
					//Orient the cards to certain angle
					angle = angle + Angle_between_players;
					Seat.style.transform = 'rotate('+angle+'deg)';
					
					//Position the players so they are in a circle clockwise
					Seat.style.top = (42.5+42.5*Math.cos(2*Math.PI*angle/360)) +"%";
					Seat.style.left = (40-42.5*Math.sin(2*Math.PI*angle/360)) +"%";
					
					//add seat to table
					play_table.appendChild(Seat);
				}
			//Only draw the new card because the other cards are already on the table
			}else{
				document.title = "Sterf"+Domme_counter2++;
				seat_number = players[id].seat;
				var Seat_id = "seat"+seat_number;
				var Seat = document.getElementById(Seat_id);
				
				Childs_of_Seat = Seat.childNodes;
				document.title = "Val dood"+Childs_of_Seat.length;
				
				var next_card = document.createElement('img');
				next_card.id = "card"+(players[id].cards.length-1);
				next_card.style.left = (players[id].cards.length-2)*20+"%"
				next_card.className = "card";
						
				//add images to cards
				next_card.src= players[id].cards[players[id].cards.length-1].path;
				
				//card gets bigger when you hover over it
				next_card.onmouseover = function(){this.className = "card_hover";};
				next_card.onmouseout = function(){this.className = "card";};
				
				//Add card to seat
				Seat.appendChild(next_card);
			}
			
			
			
			
			break;
	}
	
});

socket.on("table lobby", function(players, amount_of_players){
	var play_table = document.getElementById('table');
	var path_for_backside = "/static/Pictures/Backside.png";
	
	document.title = "Lobby";
	
	//Declare variable
	var seat_number;

	//Calculate Angle between players and 
	// setting the starting value for drawing the players at different angles
	var Angle_between_players = 360/amount_of_players;
	var angle = (amount_of_players - players[socket.id].seat)*Angle_between_players;
	
	//clear the whole table
	while (play_table.hasChildNodes()) {  
		play_table.removeChild(play_table.firstChild);
	}
	
	// add all the seats
	for (var id in players){
		//Create seat with according seat number
		seat_number = players[id].seat;
		var Seat = document.createElement("div");
		Seat.id = "seat"+seat_number;
		Seat.className = "seat";
		
		//Create nametag to show name of the player above the seat 
		var name_tag = document.createElement("p");
		name_tag.style.position = "absolute";
		name_tag.style.top = "-35%";
		name_tag.innerHTML = players[id].name;
		
		//Add nametag to the seat
		Seat.appendChild(name_tag);
		
		
		//Starting percentage
		var percentage_for_left = 0;
		
		//Create all the images in the hand and add them to the seat
		var amount_of_cards = players[id].cards.length-1
		for(var j = 1; j <= 4; j++){
			//create card with proper id and styling
			var card = document.createElement("img");
			card.id = "card"+j;
			card.style.left = percentage_for_left+"%"
			percentage_for_left = percentage_for_left + 20;
			card.className = "card";
			
			//add images to cards
			path_for_backside = players[id].cards[0].path;
			card.src= path_for_backside;
			
			//card gets bigger when you hover over it
			card.onmouseover = function(){this.className = "card_hover";};
			card.onmouseout = function(){this.className = "card";};
			
			//Add card to seat
			Seat.appendChild(card);
		} 
		
		
		//Orient the cards to certain angle
		angle = angle + Angle_between_players;
		Seat.style.transform = 'rotate('+angle+'deg)';
		
		//Position the players so they are in a circle clockwise
		Seat.style.top = (42.5+42.5*Math.cos(2*Math.PI*angle/360)) +"%";
		Seat.style.left = (40-42.5*Math.sin(2*Math.PI*angle/360)) +"%";
		
		//add seat to table
		play_table.appendChild(Seat);
	}	
	
});

socket.on("table kaarten reset", function(players, amount_of_players){
	//Get reference to div
	var play_table = document.getElementById('table');
	var path_for_backside = "/static/Pictures/Backside.png";

	
	var seat_number;
		
	//Calculate Angle between players and 
	// setting the starting value for drawing the players at different angles
	var Angle_between_players = 360/amount_of_players;
	var angle = (amount_of_players - players[socket.id].seat)*Angle_between_players;
	
	while (play_table.hasChildNodes()) {  
		play_table.removeChild(play_table.firstChild);
	}
	
	
	// add all the seats
	for (var id in players){
		//Create seat with according seat number
		seat_number = players[id].seat;
		var Seat = document.createElement("div");
		Seat.id = "seat"+seat_number;
		Seat.className = "seat";
		
		//Create nametag to show name of the player above the seat 
		var name_tag = document.createElement("p");
		name_tag.style.position = "absolute";
		name_tag.style.top = "-35%";
		name_tag.innerHTML = players[id].name;
		
		//Add nametag to the seat
		Seat.appendChild(name_tag);
		
		
		//Starting percentage
		
		//Create all the images in the hand and add them to the seat
		var amount_of_cards = players[id].cards.length-1
		for(var j = 1; j <= amount_of_cards; j++){
			//create card with proper id and styling
			var card = document.createElement("img");
			card.id = "card"+j;
			card.style.left = (j-1)*20+"%"
			card.className = "card";
			
			//add images to cards
			card.src= players[id].cards[j].path;
			
			//card gets bigger when you hover over it
			card.onmouseover = function(){this.className = "card_hover";};
			card.onmouseout = function(){this.className = "card";};
			
			//Add card to seat
			Seat.appendChild(card);
		} 
	
	
		//Orient the cards to certain angle
		angle = angle + Angle_between_players;
		Seat.style.transform = 'rotate('+angle+'deg)';
		
		//Position the players so they are in a circle clockwise
		Seat.style.top = (42.5+42.5*Math.cos(2*Math.PI*angle/360)) +"%";
		Seat.style.left = (40-42.5*Math.sin(2*Math.PI*angle/360)) +"%";
		
		//add seat to table
		play_table.appendChild(Seat);
	}
	
});

socket.on("table kaarten next card", function(players, id){	
	document.title = "Sterf"+Domme_counter2++;
	seat_number = players[id].seat;
	var Seat_id = "seat"+seat_number;
	var Seat = document.getElementById(Seat_id);
	
	Childs_of_Seat = Seat.childNodes;
	document.title = "Val dood"+Childs_of_Seat.length;
	
	var next_card = document.createElement('img');
	next_card.id = "card"+(players[id].cards.length-1);
	next_card.style.left = (players[id].cards.length-2)*20+"%"
	next_card.className = "card";
			
	//add images to cards
	next_card.src= players[id].cards[players[id].cards.length-1].path;
	
	//card gets bigger when you hover over it
	next_card.onmouseover = function(){this.className = "card_hover";};
	next_card.onmouseout = function(){this.className = "card";};
	
	//Add card to seat
	Seat.appendChild(next_card);
});

socket.on("table pyramide reset", function(players, amount_of_players){
	//Get reference to div
	var play_table = document.getElementById('table');
	var path_for_backside = "/static/Pictures/Backside.png";

	
	var seat_number;
		
	//Calculate Angle between players and 
	// setting the starting value for drawing the players at different angles
	var Angle_between_players = 360/amount_of_players;
	var angle = (amount_of_players - players[socket.id].seat)*Angle_between_players;
	
	
	
	while (play_table.hasChildNodes()) {  
		play_table.removeChild(play_table.firstChild);
	}
	
	//Declare variable
	var seat_number;

	//Calculate Angle between players and 
	// setting the starting value for drawing the players at different angles
	var Angle_between_players = 360/amount_of_players;
	var angle = (amount_of_players - players[socket.id].seat)*Angle_between_players;
	
	// add all the seats
	for (var id in players){
		//Create seat with according seat number
		seat_number = players[id].seat;
		var Seat = document.createElement("div");
		Seat.id = "seat"+seat_number;
		Seat.className = "seat_pyramide";
		
		//Create nametag to show name of the player above the seat 
		var name_tag = document.createElement("p");
		name_tag.style.position = "absolute";
		name_tag.style.top = "-50%";
		name_tag.innerHTML = players[id].name;
		
		//Add nametag to the seat
		Seat.appendChild(name_tag);
		
		
		//Starting percentage
		var percentage_for_left = 0;
		
		//Create all the images in the hand and add them to the seat
		var amount_of_cards = players[id].cards.length-1
		for(var j = 1; j <= amount_of_cards; j++){
			//create card with proper id and styling
			var card = document.createElement("img");
			card.id = "card"+j;
			card.style.left = (j-1)*20+"%"
			card.className = "card";
			
			//add images to cards
			card.src= players[id].cards[j].path;
			
			//card gets bigger when you hover over it
			card.onmouseover = function(){this.className = "card_pyramide_hover";};
			card.onmouseout = function(){this.className = "card";};
			
			//Add card to seat
			Seat.appendChild(card);
		} 
		
		
		//Orient the cards to certain angle
		angle = angle + Angle_between_players;
		Seat.style.transform = 'rotate('+angle+'deg)';
		
		//Position the players so they are in a circle clockwise
		Seat.style.top = (50-5+45*Math.cos(2*Math.PI*angle/360)) +"%";
		Seat.style.left = (50-6.5-45*Math.sin(2*Math.PI*angle/360)) +"%";
		
		//add seat to table
		play_table.appendChild(Seat);
	}	
	
	
	
});

socket.on("table pyramide empty", function(pyramide_cards){
	//place the boat
	var play_table = document.getElementById('table');
	var path_for_backside = "/static/Pictures/Backside.png";
	
	document.title = "Empty pyramide";
	
	const pos_max_left = 25;
	const pos_max_right = 95;

	const pos_max_top = 20;
	const pos_max_bottom = 80;

	for(var rij_index = 1; rij_index <= 6; rij_index++){
		if(rij_index < 6){
			//eerste 5 rijen
			for(var kaart_index = 1; kaart_index <= rij_index; kaart_index++){
				var card_div = document.createElement('div');
				card_div.id = "pyramide_card"+rij_index+"_"+kaart_index;
				
				var card_div_plaatje = document.createElement('img');
				card_div_plaatje.src = pyramide_cards[rij_index*10+kaart_index].path;
				card_div_plaatje.style.width = "100%";
				card_div_plaatje.style.height = "100%";
				card_div_plaatje.style.position = "absolute";
				card_div.appendChild(card_div_plaatje);
				
				card_div.style.width = "10%";
				card_div.style.height = "15%";
				card_div.style.position = "absolute";
				card_div.style.top = (47.5-(rij_index-2*(kaart_index-1))*((pos_max_bottom-pos_max_top)/10) )+"%";
				card_div.style.left = ((pos_max_right-2*((pos_max_right-pos_max_left)/15))-2.5*rij_index*((pos_max_right-pos_max_left)/15))+"%";

				card_div.style.transform = "rotate(90deg)";
				card_div.style.margin = "0";
				card_div.style.padding = "0";
				
				if((rij_index == 3 && kaart_index == 2)||(rij_index == 5 && (kaart_index == 2 || kaart_index == 4))){
					card_div.style.transform = "rotate(0deg)";
				}
				
				play_table.appendChild(card_div);
			}
		}else{
			//laatste kaart
			var card_div = document.createElement('div');
			card_div.id = "pyramide_card"+rij_index+"_1";
				
			var card_div_plaatje = document.createElement('img');
			card_div_plaatje.src = path_for_backside;
			card_div_plaatje.style.width = "100%";
			card_div_plaatje.style.height = "100%";
			card_div_plaatje.style.position = "absolute";
			card_div.appendChild(card_div_plaatje);
			
			card_div.style.width = "10%";
			card_div.style.height = "15%";
			card_div.style.position = "absolute";
			card_div.style.top = (47.5-(rij_index-2*(3.5-1))*((pos_max_bottom-pos_max_top)/10) )+"%";
			card_div.style.left = ((pos_max_right-2*((pos_max_right-pos_max_left)/15))-2.5*rij_index*((pos_max_right-pos_max_left)/15))+"%";

			card_div.style.margin = "0";
			card_div.style.padding = "0";
			
			play_table.appendChild(card_div);
		}
	}
});

var pyramide_card_number = 1;
var pyramide_row_number = 1;

socket.on("table pyramide next card", function(card_path, is_adt_gelegd){
	
	
	document.title = "Haal kaart ff weg (pyramide_card"+pyramide_row_number+"_"+pyramide_card_number+")";
	
	var current_pyramide_card = document.getElementById("pyramide_card"+pyramide_row_number+"_"+pyramide_card_number);
	current_pyramide_card.removeChild(current_pyramide_card.firstChild);
	
	var current_pyramide_card_plaatje = document.createElement("img");
	current_pyramide_card_plaatje.style.width = "100%";
	current_pyramide_card_plaatje.style.height = "100%";
	current_pyramide_card_plaatje.style.position = "absolute";
	current_pyramide_card_plaatje.src = card_path;
	current_pyramide_card.appendChild(current_pyramide_card_plaatje);
	
	pyramide_card_number++;
	if (pyramide_card_number > pyramide_row_number){
		pyramide_row_number++;
		pyramide_card_number = 1;
	}
	
	if(pyramide_row_number == 6 && !is_adt_gelegd){
		pyramide_card_number = 1;
	}
});

socket.on("table pyramide redraw hand", function(players, id){
	var seat_number = players[id].seat;
	var seat = document.getElementById("seat"+seat_number);
	
	while(seat.hasChildNodes()){
		seat.removeChild(seat.firstChild);
	}
	
	//Create nametag to show name of the player above the seat 
		var name_tag = document.createElement("p");
		name_tag.style.position = "absolute";
		name_tag.style.top = "-50%";
		name_tag.innerHTML = players[id].name;
		
		//Add nametag to the seat
		seat.appendChild(name_tag);
	
	var amount_of_cards = players[id].cards.length-1
	for(var j = 1; j <= amount_of_cards; j++){
		//create card with proper id and styling
		var card = document.createElement("img");
		card.id = "card"+j;
		card.style.left = (j-1)*20+"%"
		card.className = "card";
		
		//add images to cards
		card.src= players[id].cards[j].path;
		
		//card gets bigger when you hover over it
		card.onmouseover = function(){this.className = "card_hover";};
		card.onmouseout = function(){this.className = "card";};
		
		//Add card to seat
		seat.appendChild(card);
	} 
	
});

socket.on("table pyramide put card on the pyramide", function(row_number, card_number, image_path){
	document.title = row_number+"_"+card_number;
	var card_div = document.getElementById("pyramide_card"+row_number+"_"+card_number);
	
	var card_div_plaatje = document.createElement('img');
	card_div_plaatje.src = image_path;
	document.title = row_number+"_"+card_number+"(4)";
	card_div_plaatje.style.width = "100%";
	card_div_plaatje.style.height = "100%";
	card_div_plaatje.style.position = "absolute";
	var menselijke_rotatie = -8+(Math.random() * 16);
	card_div_plaatje.style.transform = "rotate("+menselijke_rotatie+"deg)";
	
	document.title = row_number+"_"+card_number+"(4)";
	card_div.appendChild(card_div_plaatje);
	
});