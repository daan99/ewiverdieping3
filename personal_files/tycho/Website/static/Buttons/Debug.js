//Set name	
document.getElementById('button_set_name').addEventListener("click", function(){
	socket.emit('set name', socket.id, document.getElementById('input_name_input').value );
})

// Show hand in terminal
document.getElementById('show_hand_in_terminal').addEventListener("click", function(){
	socket.emit('show hand in terminal', socket.id);
})

// Add card
document.getElementById('add_card_to_my_hand').addEventListener("click", function(){
	socket.emit('add card to my hand', socket.id);
})

// Reassign seats
document.getElementById('reassign_seats').addEventListener("click", function(){
	socket.emit('reassign seats');
})

//Stop Callback
document.getElementById('StopCallback').addEventListener("click", function(){
	socket.emit('stop table', socket.id);
});


document.getElementById('reset_all_cards').addEventListener("click", function(){
	socket.emit('reset all cards', socket.id);
});
