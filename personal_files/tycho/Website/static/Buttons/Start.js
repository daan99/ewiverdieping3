//Start Button
var Start_Button = document.getElementById('Start');


Start_Button.innerText = "Start";
Start_Button.style.width = 50;
Start_Button.style.height = 50;

function startGame() {
	if(enable_movement){
		enable_movement = 0;
		Start_Button.innerText = "Start";
	}else{
		enable_movement = 1;
		Start_Button.innerText = "Stop";
	}
	
	socket.emit('start');
};

Start_Button.addEventListener("click", function(){startGame();});