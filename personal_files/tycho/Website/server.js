// Dependencies
var express = require('express');
var http = require('http');
var path = require('path');
var socketIO = require('socket.io');

var app = express();
var server = http.Server(app);
var io = socketIO(server);

app.set('port', 8080);
app.use('/static', express.static(__dirname + '/static'));

// Routing
app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname, 'index.html'));
});

// Starts the server.
server.listen(8080, function() {
  console.log('Starting server on port 8080');
});




// Add the WebSocket handlers
io.on('connection', function(socket) {
});

/* setInterval(function() {
  io.sockets.emit('message', 'hi!');
}, 1000); */


var deck_tools = require('./MyLibrary/deck_functions.js')
var table_tools = require('./MyLibrary/table_functions.js')


const unshuffled_deck = deck_tools.createDeck();
deck = unshuffled_deck;
deck = deck_tools.shuffleDeck(deck);

const placeholder_card = {
	suit: 0,
	number: 0,
	path: "static/Pictures/Backside.png"
}



//deck_tools.showDeck(deck);




var players = {};
var amount_of_players = 0;

var enable_movement = 0;
var enable_Table = 0;


var current_seat = 0;
var current_round = 0;
var seat_division = [];

var choice_is_made = 0;

const states = ["Kaarten", "Pyramide", "Kaarten trekken voor boot", "Boot"];
var state = "Lobby";

var pyramide_row_number = 0;
var pyramide_card_number = 0;
var pyramide_cards = [];
var is_adt_gelegd = false;

for(var i = 1; i <= 6; i++){
	if(i < 6){
		for(var j = 1; j <= i; j++){
			pyramide_cards[i*10+j] = placeholder_card;
		}
	}else{
		pyramide_cards[i*10+1] = placeholder_card;
	}
}

var aantal_kaarten_boot = 0;
var aantal_gesloten_boot = 0;


io.on('connection', function(socket) {
	//Generate new player
	socket.on('new player', function() {
		players[socket.id] = {
			seat: 0,
			cards: [placeholder_card],
			name: socket.id,
			dealer: 0
		};
		
		console.log("New player with socket:" + socket.id);
		
		switch(state){
			case "Lobby":
				
				amount_of_players = table_tools.countPlayers(players);
				
				setTimeout(function(){io.sockets.emit('table lobby', players, amount_of_players);},500);
		
				seat_division = table_tools.seat_divider(players);
				players = table_tools.give_players_their_seats(players, seat_division);
				break;
				
			case "Kaarten":
				amount_of_players = table_tools.countPlayers(players);
				
				setTimeout(function(){io.sockets.emit('table kaarten reset', players, amount_of_players);},500);
		
				seat_division = table_tools.seat_divider(players);
				players = table_tools.give_players_their_seats(players, seat_division);
			
			
				break;
				
			case "Pyramide":
			
			
				break;
		}
		
		
		
		
		
		
	});
	
	
	//Player Disconnects
	socket.on('disconnect', ()  => {
		console.log("Player Disconnected ("+socket.id+")")
		delete players[socket.id];
		switch(state){
			case "Lobby":
				
				amount_of_players = table_tools.countPlayers(players);
				
				setTimeout(function(){io.sockets.emit('table lobby', players, amount_of_players);},500);
		
				seat_division = table_tools.seat_divider(players);
				players = table_tools.give_players_their_seats(players, seat_division);
				break;
				
			case "Kaarten":
				amount_of_players = table_tools.countPlayers(players);
				
				setTimeout(function(){io.sockets.emit('table kaarten reset', players, amount_of_players);},500);
		
				seat_division = table_tools.seat_divider(players);
				players = table_tools.give_players_their_seats(players, seat_division);
			
			
				break;
				
			case "Pyramide":
			
			
				break;
		}
	});
	
	//Start Button is pressed
	socket.on('start', function() {
		state = states[0];
		// give player their seats according to the amount of players currently online
		seat_division = table_tools.seat_divider(players);
		players = table_tools.give_players_their_seats(players, seat_division);
		
		var max_shuffle_index = new Date().getTime()/10000000000;
		console.log("Start shuffling ("+max_shuffle_index+")");
		for(var shuffle_index = 1; shuffle_index <= max_shuffle_index; shuffle_index++){
			deck = deck_tools.shuffleDeck(deck);
		}
		console.log("Done shuffling");
		

		
		current_seat = 1;
		current_round = 1;
		
		console.log("Reset table (Kaarten)")
		setTimeout(function(){io.sockets.emit('table kaarten reset', players, amount_of_players);},500);
		
		setTimeout(function(){
				io.sockets.emit('choice to client', players[seat_division[current_seat -1]].name, "");
			}, 500);
	});
	
	socket.on('reassign seats', function(){
		// give player their seats according to the amount of players currently online
		seat_division = table_tools.seat_divider(players);
		players = table_tools.give_players_their_seats(players, seat_division);
	});
	
	socket.on('add card to my hand', function(id){
		//put card in hand
		players = deck_tools.addCardToHand(players, id, deck[0]);
		deck.splice(0,1);
	});
	
	// 
	socket.on('next card', function(id){
		switch(state){
			//Krijg de eerste vier kaarten per persoon
			case states[0]:

				var current_card = deck[0];
				
				if(seat_division[current_seat - 1] == id && choice_is_made){
					players = deck_tools.addCardToHand(players, id, deck[0]);
			
					io.to(id).emit('update cards on screen', players, id);
			
					io.sockets.emit('turn over next card', deck[0].path);
					console.log(deck[0].path);
					choice_is_made = 0;
					deck.splice(0,1);
					
					//Determine the next turn
					current_seat = current_seat + 1;
			
					//New round?
					if(current_seat > amount_of_players){ 
						//Check if all players have the right amount of cards
						var allow_next_round = true;
						for(id in players){
							if(players[id].cards.length < current_round+1){
								allow_next_round = false;
								current_seat = players[id].seat;
								io.sockets.emit('choice to client', players[id].name, "");
								setTimeout(function(){io.sockets.emit('choice to client', players[id].name, "");}, 2000);
								break;
							}
						}
				
						if(allow_next_round){
							current_seat = 1;
							current_round = current_round + 1;
						}
					}
					
					setTimeout(function(){io.sockets.emit('table kaarten next card', players, id);},500);
					// Go to pyramide state
					if(current_round > 4){
						state = states[1];
						console.log("It's time for the pyramide");
						pyramide_row_number = 0;
						pyramide_card_number = 0;
						//Draw the table for the pyramide rounds
						setTimeout(function(){io.sockets.emit('table pyramide reset', players, amount_of_players);},3000);
						setTimeout(function(){io.sockets.emit('table pyramide empty', pyramide_cards);},3000);
					}
						
					//Say new name in choices box
					setTimeout(function(){io.sockets.emit('choice to client', players[seat_division[current_seat-1]].name, "");}, 2000);
			
				};
				break;
			// Draai de pyramide kaart om
			case "Pyramide":
				if(seat_division[current_seat - 1] == id){
					pyramide_card_number++;
					if (pyramide_card_number > pyramide_row_number){
						pyramide_row_number++;
						pyramide_card_number = 1;
					}
					if(pyramide_row_number == 6){
						pyramide_card_number = 1;
					}
					
					var current_card = deck[0];
					
					
					io.sockets.emit('turn over next card', deck[0].path);
					io.sockets.emit('table pyramide next card', deck[0].path, is_adt_gelegd);
					
					pyramide_cards[pyramide_row_number*10+pyramide_card_number] = deck[0];
					
					
					deck.splice(0,1);
					
				}
				break;
		}
		
		
	});
	
	socket.on("Card to pyramide", function(id, card_number){
		
		var actual_number = card_number.split("hand_card")[1];
		var kaarten_trek_deck = [];
		var kaarten_trek_deck_leeg = [];
		var meeste_kaarten = 0;
		var speler_met_meeste_kaarten; 
		var spelers_met_meeste_kaarten = [];
		
		if (state == "Pyramide"){
			var pyramide_card_card_number = pyramide_cards[pyramide_row_number*10+pyramide_card_number].number;
			var pyramide_player_card_number =  players[id].cards[actual_number].number;
			
			if(pyramide_card_card_number == pyramide_player_card_number){
				console.log("Kaart " +players[id].cards[actual_number].number + players[id].cards[actual_number].suit +" wordt gelegd ("+card_number+")");
				io.sockets.emit("table pyramide put card on the pyramide", pyramide_row_number, pyramide_card_number, players[id].cards[actual_number].path);
				console.log("P("+pyramide_row_number+")  C("+pyramide_card_number+")");
				players[id].cards.splice(actual_number, 1);
				io.sockets.emit("table pyramide redraw hand", players, id);
				io.to(id).emit('update cards on screen', players, id);
				
				
				//Laatste kaart van pyramide
				if(pyramide_row_number == 6){
					is_adt_gelegd = true;
					
					var next_round = true;
					for(var player_id in players){
						
						for (card_index = 1; card_index < players[player_id].cards.length; card_index++){
							if(players[player_id].cards[card_index].number == pyramide_card_card_number){
								next_round = false;
							}
						}
					}
						
					if(next_round){	
						for(var player_id in players){
							console.log(players[player_id].name + " heeft " + (players[player_id].cards.length - 1) + " kaarten")
							if((players[player_id].cards.length - 1) > meeste_kaarten){
								// De meeste kaarten
								speler_met_meeste_kaarten = player_id;
								meeste_kaarten = (players[player_id].cards.length - 1);
								spelers_met_meeste_kaarten = [];
							}else if((players[player_id].cards.length - 1) == meeste_kaarten){
								//multiple player with a lot of cards
								spelers_met_meeste_kaarten.push(player_id);
							}else{
								//Niet de meeste kaart
							}
							players[player_id].cards = [placeholder_card];
						}
						
						if(spelers_met_meeste_kaarten.length > 0){
							//Kaarten trekken
							console.log("Kaarten trekken")
						}else{
							//speler_met_meeste_kaarten gaat de boot in
							console.log(players[speler_met_meeste_kaarten].name + " gaat de boot in");
							
							deck = unshuffled_deck;
							deck = deck_tools.shuffleDeck(deck);
							
							
							for (var kaart_index = 0; kaart_index <= 52; kaart_index++){
								kaarten_trek_deck.push(deck[kaart_index]);
								kaarten_trek_deck_leeg.push(placeholder_card);
							}
							
							state = "Kaarten trekken voor boot";
							console.log("D("+kaarten_trek_deck.length+"), LD("+kaarten_trek_deck_leeg.length+")");
							
							players[speler_met_meeste_kaarten].cards = kaarten_trek_deck_leeg;
							
							io.to(speler_met_meeste_kaarten).emit('update cards on screen', players, speler_met_meeste_kaarten);
							
						}
					
					}
				}
			};
		}else if(state == "Kaarten trekken voor boot"){
			console.log("K("+card_number+")");
			//
		};
		
	});
	
	
	socket.on('choice', function (id, choice) {
		console.log(players[id].name +" said "+choice);
		if(seat_division[current_seat - 1] == id){
			io.sockets.emit('choice to client', players[id].name, choice);
			choice_is_made = 1;
		};
	});
	
	socket.on('set name', function(id, name){
		players[id].name = name;
		console.log("Player "+id+" now has name: "+name);
		switch(state){
			case "Lobby":
				setTimeout(function(){io.sockets.emit('table lobby', players, amount_of_players);},500);
				break;
			
			case "Kaarten":
				setTimeout(function(){io.sockets.emit('table kaarten reset', players, amount_of_players);},500);
				break;
				
			case "Pyramide":
				setTimeout(function(){io.sockets.emit('table pyramide reset', players, amount_of_players);},500);
				setTimeout(function(){io.sockets.emit('table pyramide empty', pyramide_cards);},3000);

				break;
		}
		setTimeout(function(){io.sockets.emit('table reset', players, amount_of_players, state);},500);
	});
	
	socket.on('show hand in terminal', function(id) {
		amount_of_cards_in_hand = players[id].cards.length - 1;
		
		console.log(players[id].name+ " has the following cards ("+ amount_of_cards_in_hand +")");
		for(var i = 1; i <= amount_of_cards_in_hand; i++){
			console.log(players[id].cards[i].suit + players[id].cards[i].number );
		}
		
	});
	
	socket.on('reset all cards', function() {
		deck = unshuffled_deck;
		deck = deck_tools.shuffleDeck(deck);
		
		for(var id in players){
			players[id].cards = [placeholder_card];
		}
		setTimeout(function(){io.sockets.emit('table lobby', players, amount_of_players);},500);
		
		var current_seat = 0;
		var current_round = 0;
		var seat_division = [];

		var choice_is_made = 0;

		const states = ["Kaarten", "Pyramide", "Kaarten trekken voor boot", "Boot"];
		var state = "Lobby";

		var pyramide_row_number = 0;
		var pyramide_card_number = 0;
		var pyramide_cards = [];
		var is_adt_gelegd = false;

		for(var i = 1; i <= 6; i++){
			if(i < 6){
				for(var j = 1; j <= i; j++){
					pyramide_cards[i*10+j] = placeholder_card;
				}
			}else{
				pyramide_cards[i*10+1] = placeholder_card;
			}
		}

		var aantal_kaarten_boot = 0;
		var aantal_gesloten_boot = 0;

		
	});
	
	socket.on('stop table', function() {
		if( enable_Table ){
			enable_Table = 0;
			console.log('Stop the table update')
		}else{
			enable_Table = 1;
			console.log('Start the table update')
		};
		setTimeout(function(){io.sockets.emit('table reset', players, amount_of_players, state);},500);
		
	});
});

//Send table updates
setInterval(function() {
	if(enable_Table){
		io.sockets.emit('table reset', players, amount_of_players, state);
		console.log("There are " + amount_of_players + " at the table")
		
	}
 }, 1000);
 
 setInterval(
	function() {
		for(var id in players){
			io.to(id).emit('You are allowed to communicate with the server');
		}
	}

,1000);

