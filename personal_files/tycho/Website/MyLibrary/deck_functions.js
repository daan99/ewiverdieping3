module.exports = {
	//Create the deck
	createDeck: function() {
		const suits = ["Hearts", "Diamonds", "Spades", "Clubs"];
		const numbers = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];

		var deck = [];
		i = 0;
		for (var color in suits){
			for(var number in numbers){
				deck[i] = {
					suit: suits[color],
					number: numbers[number],
					path: "./static/Pictures/Cards/"+color+"_"+numbers[number]+".png"
				}
				i = i + 1;
			}
		}
		
		return deck
	},
	
	//Shuffle the playing cards
	shuffleDeck: function(deck) {
        amount_of_playing_cards = deck.length;
	
			for (i = 0; i < amount_of_playing_cards; i++){
				var random_index = Math.floor(Math.random()*(amount_of_playing_cards - i));
		
				temp_storage = deck[i];
				deck[i] = deck[random_index];
				deck[random_index] = temp_storage;
			}
	
		return deck;
    },
	
	//Display the deck in the console
	showDeck: function(deck) {
		for (var card in deck){
			console.log("Card is " + deck[card].number + " of " + deck[card].suit);
		};
	},
	
	//
	addCardToHand: function(players, id, card){
		var amount_of_cards_in_hand = players[id].cards.length;
		
		players[id].cards[amount_of_cards_in_hand] = card;
		console.log(players[id].name + " got card " + card + (players[id].cards.length));
		
		return players
	}
	
	
};