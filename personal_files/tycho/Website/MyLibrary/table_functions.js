module.exports = {
	//Create the deck
	countPlayers: function(players) {
		amount_of_players = 0;
		
		for (var player in players){
			amount_of_players++;
		};
		
		return amount_of_players
	},
	
	//Hand out the seat to all the players
	seat_divider: function(players){
		seat_division = [];
		for(var id in players){
			seat_division.push(id);
		}
		
		return(seat_division)
	},
	
	
	//
	give_players_their_seats: function(players, seat_division){
		for(var playerid in players){
			for(var i = 0; i < seat_division.length; i++){
				if(playerid == seat_division[i]){
					players[playerid].seat = i+1;
				}
			}
		}
		
		return players;
	}
};