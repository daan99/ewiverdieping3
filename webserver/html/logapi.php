<?php
$olderdata = $_GET["od"];
$delimiters = explode(",", $_GET["del"]);

$online = 0;
$players = [];

$uptimes = [];

// API functions

function extract_player($line,$word) {
	$words = explode(" ", $line);
        $loc = array_search($word,$words);

        return $words[$loc-1];
}

function push_time($type, $name, $line) {
	global $uptimes;
	$time = array_slice(str_split($line),1,8);

	if(!array_key_exists($name,$uptimes)) $uptimes[$name] = ['joined' => [], 'left' =>[]];
	if($type == 'j') array_push($uptimes[$name]['joined'],$time);
        else if($type == 'l') array_push($uptimes[$name]['left'],$time);
}

function calc_times($textjoin,$textleave) {
	$hoursjoin = implode("", array_slice($textjoin,0,2)) * 60 * 60;
	$minutesjoin = implode("", array_slice($textjoin,3,2)) * 60;
	$secondsjoin = implode("", array_slice($textjoin,6,2));
	$hoursleave = implode("", array_slice($textleave,0,2)) * 60 * 60;
	$minutesleave = implode("", array_slice($textleave,3,2)) * 60;
	$secondsleave = implode("", array_slice($textleave,6,2));

	$totalsecondsjoin = $hoursjoin + $minutesjoin + $secondsjoin;
	$totalsecondsleave = $hoursleave + $minutesleave + $secondsleave;

	return $totalsecondsleave - $totalsecondsjoin;
}

function get_time($name) {
	$joindata = $uptimes[$name]['joined'];
	$leavedata = $uptimes[$name]['left'];
	$septimes = [];

	for($i = 0; $i < sizeOf($leavedata); $i++) $septimes[$i] = calc_times($joindata[$i],$leavedata[$i]);
	if(sizeOf($joindata) > sizeOf($leavedata)) {}
}

function get_uptimes() {
	$usernames = array_keys($uptimes);
	$outputtimes = [];
	foreach($usernames as $username) $outputtimes[$username] = get_time($username);

	return $outputtimes;
}

function make_text($joined, $left) {
	$joined_txt = "** heeft de server gejoined!\n";
	$left_txt = "** heeft de server geleaved!\n";

	$output = "";

	foreach($joined as $join) $output = $output . "**" . $join . $joined_txt;
	foreach($left as $leave) $output = $output . "**" . $leave . $left_txt;

	return $output;
}

// Main API
	$log = file('../../../root/ftb/logs/latest.log');
	foreach($log as $line) {
		if(strpos($line, "[net.minecraft.server.dedicated.DedicatedServer]")) {
			if(strpos($line, "joined")) {
				$name = extract_player($line,"joined");
				array_push($players, $name);
				push_time('j', $name, $line);
			}
			if(strpos($line, "left")) {
				$name = extract_player($line,"left");
				array_splice($players, array_search($name,$players), 1);
				push_time('l', $name, $line);
			}
		}
	}
	sort($players);
	$online = sizeOf($players);
	$outputarray = ['online' => $online, 'players' => $players];
	if(!is_bool(array_search("ta",$delimiters))) $outputarray['times'] = $uptimes;
	if(!is_bool(array_search("t",$delimiters))) $outputarray['uptime'] = get_uptimes();

	if($olderdata) {
		$joined = [];
		$left = [];
		$olderarray = explode(",", urldecode($olderdata));
		foreach($olderarray as $oldplayer) if(is_bool(array_search($oldplayer,$players))) array_push($left, $oldplayer);
		foreach($players as $newplayer) if(is_bool(array_search($newplayer,$olderarray))) array_push($joined, $newplayer);

		$outputarray['statustext'] = make_text($joined, $left);
	} else if($olderdata == 0) {
		$joined = [];
		$left = [];
		$olderarray = explode(",", urldecode($olderdata));
		foreach($players as $newplayer) if(is_bool(array_search($newplayer,$olderarray))) array_push($joined, $newplayer);

		$outputarray['statustext'] = make_text($joined, $left);
	}
        echo(json_encode($outputarray));
?>
