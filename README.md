# EwiVerdieping3 Server files

Deze git bevat alle (nuttige) files van de server.

* personal_files: bevat in alle mappen je root directory files van op de server
* ftb: bevat de gehele FTB minecraft server, voor als je deze nog zelf een keer zou willen draaien. Heeft nu alleen nog maar een file voor linux, ServerStart.sh, maar een Windows file zal ook toegevoegd worden. LET OP: voor het draaien van deze server is een maximale versie van Java JDK 8.0 nodig. Latere versies werken neer ivm ondersteuning van Forge.
* webserver: bevat de bestanden uit de webserver die draaide op de server.
* latest_backup.zip: dit is de laatste worlddownload van de Minecraft FTB server. Deze kan je in je eigen minecraft folder gooien om de server singleplayer te kunnen spelen.
